/*
GAME FUNCTION:
- Player guess a number between max and min
- Player get a certain amount guesses
- Nodify player guesses remaining
- Nodify the player the correct answer if lose
- Let player choose to play again
*/

// Game values
let min = 1,
  max = 10,
  winningNum = getRandomNum(min, max),
  guessesLeft = 3;

// UI Elements
const game = document.querySelector('#game'),
  minNum = document.querySelector('.min-num'),
  maxNum = document.querySelector('.max-num'),
  guessBtn = document.querySelector('#guess-btn'),
  guessInput = document.querySelector('#guess-input'),
  message = document.querySelector('.message');

// Assign UI min and max
minNum.textContent = min;
maxNum.textContent = max;

// Play again event listener
game.addEventListener('mousedown', function(e) {
  if (e.target.className === 'play-again') {
    window.location.reload();
  }
});

// Listen for guess
guessBtn.addEventListener('click', function() {
  let guess = parseInt(guessInput.value);

  // Validate input
  if (isNaN(guess) || guess < min || guess > max) {
    setMessage(`Please enter a number between ${min} and ${max}`, 'red');
  }

  // Check if win
  if (guess === winningNum) {
    gameOver(true, `${winningNum} is correct. YOU WIN!`);
  } else {
    // Wrong guess
    guessesLeft -= 1;

    if (guessesLeft === 0) {
      // Game over. Lost
      gameOver(false, `Game over. You lost. The correct number is ${winningNum}`);
    } else {
      // Game continues - answer wrong

      // change border color
      guessInput.style.borderColor = 'red';
      // Clear inpt
      guessInput.value = '';
      // Tell user it is the wrong number.
      setMessage(`${guess} isn't correct. ${guessesLeft} guesses left.`, 'red');
    }
  }
});

// Game over
function gameOver(won, msg) {
  let color;
  won === true ? (color = 'green') : (color = 'red');
  // Disable input
  guessInput.disabled = true;
  // change border color
  guessInput.style.borderColor = color;
  // change text color
  message.style.color = color;
  // set message for winner
  setMessage(msg);

  // Play again
  guessBtn.value = 'Play Again';
  guessBtn.className += 'play-again';
}

// Get winning number
function getRandomNum(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

// Set message
function setMessage(msg, color) {
  message.style.color = color;
  message.textContent = msg;
}
